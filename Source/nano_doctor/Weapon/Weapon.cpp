// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"

#include "Components/SkeletalMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/WidgetComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "nano_doctor/Character/NanoCharacter.h"
#include "nano_doctor/Character/NanoPlayerController.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AWeapon::AWeapon()
{
	bReplicates = true;
	AActor::SetReplicateMovement(true);

	WeaponMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponMesh"));
	
	SetRootComponent(WeaponMeshComp);

	WeaponMeshComp->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	WeaponMeshComp->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	WeaponMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	EnableCustomDepth(true);
	WeaponMeshComp->SetCustomDepthStencilValue(CUSTOM_DEPTH_BLUE);
	WeaponMeshComp->MarkRenderStateDirty();


	AreaSphere = CreateDefaultSubobject<USphereComponent>(TEXT("AreaSphere"));
	AreaSphere->SetupAttachment(RootComponent);
	AreaSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	PickupWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("PickupWidget"));
	PickupWidget->SetupAttachment(RootComponent);
}

void AWeapon::EnableCustomDepth(bool bEnable)
{
	if (WeaponMeshComp)
	{
		WeaponMeshComp->SetRenderCustomDepth(bEnable);
	}
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();

	AreaSphere->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	AreaSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	AreaSphere->OnComponentBeginOverlap.AddDynamic(this, &AWeapon::OnSphereOverlap);
	AreaSphere->OnComponentEndOverlap.AddDynamic(this, &AWeapon::OnSphereEndOverlap);

	if (PickupWidget)
	{
		PickupWidget->SetVisibility(false);
	}
}

void AWeapon::SetWeaponState(EWeaponState State)
{
	WeaponState = State;
	OnWeaponStateSet();
}

void AWeapon::OnWeaponStateSet()
{
	switch (WeaponState)
	{
	case EWeaponState::EWS_Equipped:
		OnEquipped();
		break;
	case EWeaponState::EWS_Dropped:
		OnDropped();
		break;

	default:
		break;
	}
}

void AWeapon::OnEquipped()
{
	ShowPickupWidget(false);
	AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponMeshComp->SetSimulatePhysics(false);
	WeaponMeshComp->SetEnableGravity(false);
	WeaponMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	EnableCustomDepth(false);
}

void AWeapon::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                              UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ANanoCharacter* NanoCharacter = Cast<ANanoCharacter>(OtherActor);

	if(NanoCharacter)
	{
		NanoCharacter->SetOverlappingWeapon(this);
	}
}

void AWeapon::OnSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ANanoCharacter* NanoCharacter = Cast<ANanoCharacter>(OtherActor);

	if (NanoCharacter)
	{
		NanoCharacter->SetOverlappingWeapon(nullptr);
	}
}

void AWeapon::OnRep_WeaponState()
{
	OnWeaponStateSet();
}

void AWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWeapon, WeaponState);
	DOREPLIFETIME(AWeapon, NeedEnergy);

	DOREPLIFETIME_CONDITION(AWeapon, bUseServerSideRewind, COND_OwnerOnly);
}

void AWeapon::OnRep_Owner()
{
	Super::OnRep_Owner();
	if (Owner == nullptr)
	{
		NanoOwnerCharacter = nullptr;
		NanoOwnerController = nullptr;
	}
	else
	{
		NanoOwnerCharacter = NanoOwnerCharacter == nullptr ? Cast<ANanoCharacter>(Owner) : NanoOwnerCharacter;
	}
}

void AWeapon::ShowPickupWidget(bool bShowWidget)
{
	if (PickupWidget)
	{
		PickupWidget->SetVisibility(bShowWidget);
	}
}

void AWeapon::Fire(const FVector& HitTarget)
{

}

void AWeapon::Dropped()
{
	SetWeaponState(EWeaponState::EWS_Dropped);
	FDetachmentTransformRules DetachRules(EDetachmentRule::KeepWorld, true);
	WeaponMeshComp->DetachFromComponent(DetachRules);
	SetOwner(nullptr);
	NanoOwnerCharacter = nullptr;
	NanoOwnerController = nullptr;
}

void AWeapon::OnDropped()
{
	if (HasAuthority())
	{
		AreaSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	WeaponMeshComp->SetSimulatePhysics(true);
	WeaponMeshComp->SetEnableGravity(true);
	WeaponMeshComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	WeaponMeshComp->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	WeaponMeshComp->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	WeaponMeshComp->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);

	WeaponMeshComp->SetCustomDepthStencilValue(CUSTOM_DEPTH_BLUE);
	WeaponMeshComp->MarkRenderStateDirty();
	EnableCustomDepth(true);

	NanoOwnerCharacter = NanoOwnerCharacter == nullptr ? Cast<ANanoCharacter>(GetOwner()) : NanoOwnerCharacter;
	if (NanoOwnerCharacter)
	{
		NanoOwnerController = NanoOwnerController == nullptr ? Cast<ANanoPlayerController>(NanoOwnerCharacter->Controller) : NanoOwnerController;

	}
}

