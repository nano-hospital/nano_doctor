// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileWater.h"

#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "nano_doctor/Character/NanoCharacter.h"
#include "nano_doctor/Character/NanoPlayerController.h"
#include "nano_doctor/HUD/NanoHUD.h"
#include "Components/BoxComponent.h"

AProjectileWater::AProjectileWater()
{
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->SetIsReplicated(true);
	ProjectileMovementComponent->InitialSpeed = WaterSpeed;
	ProjectileMovementComponent->MaxSpeed = WaterSpeed;

	CollisionBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Block);

}

void AProjectileWater::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit)
{
	ANanoCharacter* OwnerCharacter = Cast<ANanoCharacter>(GetOwner());
	if (OwnerCharacter)
	{
		ANanoPlayerController* OwnerController = Cast<ANanoPlayerController>(OwnerCharacter->Controller);
		if (OwnerController)
		{
			if (OwnerCharacter->HasAuthority())
			{
				const float DamageToCause = Hit.BoneName.ToString() == FString("head") ? HeadShotDamage : Damage;

				if(OtherActor->ActorHasTag(TEXT("Enemy")))
				{
					UGameplayStatics::ApplyPointDamage(OtherActor, DamageToCause, GetActorLocation(), Hit, OwnerController, this, UDamageType::StaticClass());
				}
				Super::OnHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
				return;
			}

		}
	}
	
	Super::OnHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileWater::BeginPlay()
{
	Super::BeginPlay();
}
