// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "ProjectileWater.generated.h"

/**
 * 
 */
UCLASS()
class NANO_DOCTOR_API AProjectileWater : public AProjectile
{
	GENERATED_BODY()

public:
	AProjectileWater();

	virtual void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

protected:
	virtual void BeginPlay() override;
};
