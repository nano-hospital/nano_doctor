// Fill out your copyright notice in the Description page of Project Settings.


#include "BuffComponent.h"

#include "nano_doctor/Character/NanoCharacter.h"

// Sets default values for this component's properties
UBuffComponent::UBuffComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

}

void UBuffComponent::Heal(float HealAmount, float HealingTime)
{
	bHealing = true;
	HealingRate = HealAmount / HealingTime;
	AmountToHeal += HealAmount;
}

void UBuffComponent::Charge(float ChargeAmount, float ChargingTime)
{
	bCharging = true;
	ChargingRate = ChargeAmount / ChargingTime;
	AmountToCharge += ChargeAmount;
}


// Called when the game starts
void UBuffComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UBuffComponent::HealRampUp(float DeltaTime)
{
	if (!bHealing || Character == nullptr || Character->IsDied()) return;

	const float HealThisFrame = HealingRate * DeltaTime;
	Character->SetHealth(FMath::Clamp(Character->GetHealth() + HealThisFrame, 0.f, Character->GetMaxHealth()));
	Character->UpdateHUDHealth();
	AmountToHeal -= HealThisFrame;

	if (AmountToHeal <= 0.f || Character->GetHealth() >= Character->GetMaxHealth())
	{
		bHealing = false;
		AmountToHeal = 0.f;
	}
}

void UBuffComponent::ChargeRampUp_Implementation(float DeltaTime)
{
	if (!bCharging || Character == nullptr || Character->IsDied()) return;

	const float ChargeThisFrame = ChargingRate * DeltaTime;
	Character->SetEnergy(FMath::Clamp(Character->GetEnergy() + ChargeThisFrame, 0.f, Character->GetMaxEnergy()));
	Character->UpdateHUDEnergy();
	AmountToCharge -= ChargeThisFrame;

	if (AmountToCharge <= 0.f || Character->GetEnergy() >= Character->GetMaxEnergy())
	{
		bCharging = false;
		AmountToCharge = 0.f;
	}
}

// Called every frame
void UBuffComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	HealRampUp(DeltaTime);
	ChargeRampUp(DeltaTime);
}

