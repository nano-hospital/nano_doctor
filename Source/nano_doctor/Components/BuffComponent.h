// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BuffComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class NANO_DOCTOR_API UBuffComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBuffComponent();
	friend class ANanoCharacter;
	UFUNCTION(BlueprintCallable)
	void Heal(float HealAmount, float HealingTime);
	
	UFUNCTION(BlueprintCallable)
	void Charge(float ChargeAmount, float ChargingTime);
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	void HealRampUp(float DeltaTime);

	UFUNCTION(Server, Reliable)
	void ChargeRampUp(float DeltaTime);

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
private:
	UPROPERTY()
	ANanoCharacter* Character;

	/** 
	* Heal buff
	*/

	bool bHealing = false;
	float HealingRate = 0.f;
	float AmountToHeal = 0.f;

	/** 
	* Charge buff
	*/

	bool bCharging = false;
	float ChargingRate = 0.f;
	float AmountToCharge = 0.f;
		
};
