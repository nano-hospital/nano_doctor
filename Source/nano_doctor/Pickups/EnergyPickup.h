// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "EnergyPickup.generated.h"

/**
 * 
 */
UCLASS()
class NANO_DOCTOR_API AEnergyPickup : public APickup
{
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	AEnergyPickup();

protected:
	virtual void OnSphereOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult
	) override;
private:

	UPROPERTY(EditAnywhere)
	float ChargeAmount = 100.f;

	UPROPERTY(EditAnywhere)
	float ChargingTime = 1.f;
};
