// Fill out your copyright notice in the Description page of Project Settings.


#include "RoomPickup.h"

#include "nano_doctor/Character/NanoCharacter.h"

void ARoomPickup::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                  UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnSphereOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	if(OtherActor->ActorHasTag(TEXT("Player")))
	{
		ANanoCharacter* NanoCharacter = Cast<ANanoCharacter>(OtherActor);
		if (NanoCharacter)
		{
			NanoCharacter->PickupRoom();
		}

		Destroy();
	}
}
