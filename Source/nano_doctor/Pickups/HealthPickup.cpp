// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthPickup.h"

#include "nano_doctor/Character/NanoCharacter.h"
#include "nano_doctor/Components/BuffComponent.h"


// Sets default values
AHealthPickup::AHealthPickup()
{
	bReplicates = true;
}

void AHealthPickup::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnSphereOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	if(OtherActor->ActorHasTag(TEXT("Player")))
	{
		ANanoCharacter* NanoCharacter = Cast<ANanoCharacter>(OtherActor);
		if (NanoCharacter)
		{
			UBuffComponent* Buff = NanoCharacter->GetBuff();
			if (Buff)
			{
				Buff->Heal(HealAmount, HealingTime);
			}
		}

		Destroy();
	}
}


