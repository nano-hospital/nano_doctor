// Fill out your copyright notice in the Description page of Project Settings.


#include "NanoCharacter.h"

#include "NanoPlayerController.h"
#include "TimerManager.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "nano_doctor/nano_doctor.h"
#include "nano_doctor/Components/BuffComponent.h"
#include "nano_doctor/Components/CombatComponent.h"
#include "nano_doctor/GameMode/NanoGameMode.h"
#include "nano_doctor/Weapon/Weapon.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ANanoCharacter::ANanoCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(GetMesh());
	CameraBoom->TargetArmLength = 600.f;
	CameraBoom->bUsePawnControlRotation = true;

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;

	bUseControllerRotationYaw = false;
	GetCharacterMovement()->bOrientRotationToMovement = true;

	Combat = CreateDefaultSubobject<UCombatComponent>(TEXT("Combat"));
	Combat->SetIsReplicated(true);

	Buff = CreateDefaultSubobject<UBuffComponent>(TEXT("BuffComponent"));
	Buff->SetIsReplicated(true);
	
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	GetMesh()->SetCollisionObjectType(ECC_SkeletalMesh);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	
	NetUpdateFrequency = 66.f;
	MinNetUpdateFrequency = 33.f;

	CommonTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("TimelineComponent"));

}

void ANanoCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ANanoCharacter, OverlappingWeapon, COND_OwnerOnly);

	DOREPLIFETIME(ANanoCharacter, Health);
	DOREPLIFETIME(ANanoCharacter, HealthUI);
	DOREPLIFETIME(ANanoCharacter, bDisableGameplay);
	DOREPLIFETIME(ANanoCharacter, CarryRoom);
	DOREPLIFETIME(ANanoCharacter, bIsImmune);
	DOREPLIFETIME(ANanoCharacter, CarryEnforce);
	DOREPLIFETIME(ANanoCharacter, Energy);
}

void ANanoCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (Combat)
	{
		Combat->Character = this;
	}

	if (Buff)
	{
		Buff->Character = this;
	}
}

void ANanoCharacter::PlayFireMontage(bool bAiming)
{
	if (Combat == nullptr || Combat->EquippedWeapon == nullptr) return;

	if (FireWeaponMontage)
	{
		const FName SectionName = bAiming ? FName("RifleAim") : FName("RifleHip");
		PlayAnimMontage(FireWeaponMontage, 1.f, SectionName);
	}
}

void ANanoCharacter::PlayHitReactMontage()
{
	if (Combat == nullptr) return;

	if (HitReactMontage)
	{
		ResetDash();
		StopAnimMontage();
		const FName SectionName("FromFront");
		PlayAnimMontage(HitReactMontage, 1.f, SectionName);
	}
}

void ANanoCharacter::PlayDieMontage()
{
	if (DieMontage)
	{
		PlayAnimMontage(DieMontage);
	}
}

void ANanoCharacter::UpdateHUDHealth()
{
	NanoPlayerController = NanoPlayerController == nullptr ? Cast<ANanoPlayerController>(Controller) : NanoPlayerController;
	if(NanoPlayerController)
	{
		NanoPlayerController->SetHUDHealth(Health, MaxHealth);
	}
}

void ANanoCharacter::UpdateHUDEnergy()
{
	NanoPlayerController = NanoPlayerController == nullptr ? Cast<ANanoPlayerController>(Controller) : NanoPlayerController;
	if(NanoPlayerController)
	{
		NanoPlayerController->SetHUDEnergy(Energy, MaxEnergy);
	}
}

void ANanoCharacter::UpdateHUDRoom()
{
	NanoPlayerController = NanoPlayerController == nullptr ? Cast<ANanoPlayerController>(Controller) : NanoPlayerController;
	if(NanoPlayerController)
	{
		NanoPlayerController->SetHUDRoom(CarryRoom);
	}
}

void ANanoCharacter::UpdateHUDEnforce()
{
	NanoPlayerController = NanoPlayerController == nullptr ? Cast<ANanoPlayerController>(Controller) : NanoPlayerController;
	if(NanoPlayerController)
	{
		NanoPlayerController->SetHUDEnforce(CarryEnforce);
	}
}

void ANanoCharacter::Die(bool bPlayerLeftGame)
{
	DropButtonPressed();
	MulticastDie(bPlayerLeftGame);
}

void ANanoCharacter::MulticastDie_Implementation(bool bPlayerLeftGame)
{
	bLeftGame = bPlayerLeftGame;
	bDied = true;

	CarryRoom = 0;
	CarryEnforce = 0;
	PlayDieMontage();
	
	// Start dissolve effect
	if (DissolveMaterialInstance)
	{
		UE_LOG(LogTemp, Warning, TEXT("Start dissolve effect"));

		DynamicDissolveMaterialInstance = UMaterialInstanceDynamic::Create(DissolveMaterialInstance, this);
		GetMesh()->SetMaterial(0, DynamicDissolveMaterialInstance);
		DynamicDissolveMaterialInstance->SetScalarParameterValue(TEXT("Dissolve"), -0.55f);
		DynamicDissolveMaterialInstance->SetScalarParameterValue(TEXT("Glow"), 200.f);
	}
	StartDissolve();

	// Disable character movement
	bDisableGameplay = true;
	GetCharacterMovement()->DisableMovement();
	if (Combat)
	{
		Combat->FireButtonPressed(false);
	}
	// Disable collision
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetWorldTimerManager().SetTimer(
		DieTimer,
		this,
		&ANanoCharacter::DieTimerFinished,
		DieDelay
	);
}

void ANanoCharacter::Destroyed()
{
	Super::Destroyed();
}

// Called when the game starts or when spawned
void ANanoCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	UpdateHUDHealth();
	UpdateHUDEnergy();
	
	NanoPlayerController = Cast<ANanoPlayerController>(Controller);

	if (HasAuthority())
	{
		UE_LOG(LogTemp, Warning, TEXT("ReceiveDamage"));
		OnTakeAnyDamage.AddDynamic(this, &ANanoCharacter::ReceiveDamage);
	}
}

void ANanoCharacter::OnRep_OverlappingWeapon(AWeapon* LastWeapon)
{
	if (OverlappingWeapon)
	{
		OverlappingWeapon->ShowPickupWidget(true);
	}
	if (LastWeapon)
	{
		LastWeapon->ShowPickupWidget(false);
	}
}

void ANanoCharacter::EquipButtonPressed()
{
	if (bDisableGameplay) return;
	if(Combat)
	{
		if (OverlappingWeapon)
		{
			Combat->EquipWeapon(OverlappingWeapon);
		}
		ServerEquipButtonPressed();
	}
}

void ANanoCharacter::AimButtonPressed()
{
	if (bDisableGameplay) return;
	if (Combat)
	{
		Combat->SetAiming(true);
	}
}

void ANanoCharacter::AimButtonReleased()
{
	if (Combat)
	{
		Combat->SetAiming(false);
	}
}

void ANanoCharacter::FireButtonPressed()
{
	if (bDisableGameplay) return;
	if (Combat)
	{
		Combat->FireButtonPressed(true);
	}
}

void ANanoCharacter::FireButtonReleased()
{
	if (bDisableGameplay) return;
	if (Combat)
	{
		Combat->FireButtonPressed(false);
	}
}

void ANanoCharacter::DropButtonPressed()
{
	if (bDisableGameplay) return;
	if(Combat)
	{
		if (Combat->EquippedWeapon)
		{
			Combat->DropEquippedWeapon();
		}
		ServerDropButtonPressed();
	}
}

void ANanoCharacter::AimOffset(float DeltaTime)
{
	if (Combat && Combat->EquippedWeapon == nullptr) return;
	float Speed = GetVelocity().Size2D();
	bool bIsInAir = GetCharacterMovement()->IsFalling();

	if (Speed == 0.f && !bIsInAir) // standing still, not jumping
	{
		FRotator CurrentAimRotation = FRotator(0.f, GetBaseAimRotation().Yaw, 0.f);
		FRotator DeltaAimRotation = UKismetMathLibrary::NormalizedDeltaRotator(CurrentAimRotation, StartingAimRotation);
		AO_Yaw = DeltaAimRotation.Yaw;
		bUseControllerRotationYaw = true;
	}
	if (Speed > 0.f || bIsInAir) // running, or jumping
	{
		StartingAimRotation = FRotator(0.f, GetBaseAimRotation().Yaw, 0.f);
		AO_Yaw = 0.f;
		bUseControllerRotationYaw = true;
	}

	CalculateAO_Pitch();
}

void ANanoCharacter::CalculateAO_Pitch()
{
	AO_Pitch = GetBaseAimRotation().Pitch;
	if (AO_Pitch > 90.f && !IsLocallyControlled())
	{
		// map pitch from [270, 360) to [-90, 0)
		FVector2D InRange(270.f, 360.f);
		FVector2D OutRange(-90.f, 0.f);
		AO_Pitch = FMath::GetMappedRangeValueClamped(InRange, OutRange, AO_Pitch);
	}
}

void ANanoCharacter::ServerEquipButtonPressed_Implementation()
{
	if (Combat)
	{
		if (OverlappingWeapon)
		{
			Combat->EquipWeapon(OverlappingWeapon);
		}
	}
}

void ANanoCharacter::ServerDropButtonPressed_Implementation()
{
	if (Combat)
	{
		if (Combat->EquippedWeapon)
		{
			Combat->DropEquippedWeapon();
		}
	}
}

void ANanoCharacter::ReceiveDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
                                   AController* InstigatorController, AActor* DamageCauser)
{
	NanoGameMode = NanoGameMode == nullptr ? GetWorld()->GetAuthGameMode<ANanoGameMode>() : NanoGameMode;

	if (bDied || NanoGameMode == nullptr || bIsImmune) return;

	Health = FMath::Clamp(Health - Damage, 0.f, MaxHealth);
	PlayHitReactMontage();

	UE_LOG(LogTemp, Warning, TEXT("PlayHitReactMontage"));
	StartHealthTransition();
	UpdateHUDHealth();

	if (Health == 0.f)
	{
		if (NanoGameMode)
		{
			UE_LOG(LogTemp, Warning, TEXT("PlayDie!"));
			NanoGameMode->PlayerDied(this);
		}
	}
}

void ANanoCharacter::PotionDamage(float Damage)
{
	NanoGameMode = NanoGameMode == nullptr ? GetWorld()->GetAuthGameMode<ANanoGameMode>() : NanoGameMode;

	if (bDied || NanoGameMode == nullptr) return;

	Health = FMath::Clamp(Health - Damage, 0.f, MaxHealth);

	UpdateHUDHealth();

	if (Health == 0.f)
	{
		if (NanoGameMode)
		{
			UE_LOG(LogTemp, Warning, TEXT("PlayDie!"));
			NanoGameMode->PlayerDied(this);
		}
	}
}

void ANanoCharacter::PollInit()
{
	if (NanoPlayerController == nullptr)
	{
		NanoPlayerController = NanoPlayerController == nullptr ? Cast<ANanoPlayerController>(Controller) : NanoPlayerController;
		if (NanoPlayerController)
		{
			UpdateHUDHealth();
			UpdateHUDEnergy();
			UpdateHUDRoom();
			UpdateHUDEnforce();
		}
	}
	else
	{
		UpdateHUDEnergy();
	}
}

// Called every frame
void ANanoCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GetLocalRole() > ENetRole::ROLE_SimulatedProxy && IsLocallyControlled())
	{
		AimOffset(DeltaTime);
	}
	else
	{
		CalculateAO_Pitch();
	}

	HideCameraIfCharacterClose();
	PollInit();
}

void ANanoCharacter::SetOverlappingWeapon(AWeapon* Weapon)
{
	if (OverlappingWeapon)
	{
		OverlappingWeapon->ShowPickupWidget(false);
	}
	OverlappingWeapon = Weapon;
	if (IsLocallyControlled())
	{
		if (OverlappingWeapon)
		{
			OverlappingWeapon->ShowPickupWidget(true);
		}
	}
}

bool ANanoCharacter::IsWeaponEquipped() const
{
	return (Combat && Combat->EquippedWeapon);
}

bool ANanoCharacter::IsAiming()
{
	return (Combat && Combat->bAiming);
}

AWeapon* ANanoCharacter::GetEquippedWeapon() const
{
	if (Combat == nullptr) return nullptr;
	return Combat->EquippedWeapon;
}

FVector ANanoCharacter::GetHitTarget() const
{
	if (Combat == nullptr) return FVector();
	return Combat->HitTarget;
}

EWeaponType ANanoCharacter::GetEquippedWeaponType() const
{
	if(GetEquippedWeapon())
	{
		return GetEquippedWeapon()->GetWeaponType();
	}
	else
	{
		return EWeaponType::EWT_MAX;
	}
}

void ANanoCharacter::PickupRoom()
{
	CarryRoom += 1;
	UpdateHUDRoom();
}

void ANanoCharacter::UseRoom()
{
	CarryRoom -= 1;
	UpdateHUDRoom();
}

void ANanoCharacter::PickupEnforce()
{
	UE_LOG(LogTemp, Warning, TEXT("PickupEnforce"));
	CarryEnforce += 1;
	UpdateHUDEnforce();
}

void ANanoCharacter::UseEnforce()
{
	if(IsWeaponEquipped())
	{
		CarryEnforce -= 1;
		UpdateHUDEnforce();
	}
}

void ANanoCharacter::SetEnergy_Implementation(float Amount)
{
	UE_LOG(LogTemp, Warning, TEXT("SetEnergy: %f"), Amount);
	Energy = Amount;
}

void ANanoCharacter::ApplyRootMotionConstantForce(FVector WorldDirection, float Strength, float Duration)
{
	float StartTime = GetWorld()->GetTimeSeconds();
	float EndTime = StartTime + Duration;

	if(IsValid(GetCharacterMovement()))
	{
		TSharedPtr<FRootMotionSource_ConstantForce> ConstantForce = MakeShared<FRootMotionSource_ConstantForce>();
		FName ForceName = ForceName.IsNone() ? FName("ApplyRootMotionConstantForce"): ForceName;
		ConstantForce->InstanceName = ForceName;
		ConstantForce->AccumulateMode = ERootMotionAccumulateMode::Additive;
		ConstantForce->Priority = 5;
		ConstantForce->Force = WorldDirection * Strength;
		ConstantForce->Duration = Duration;
		ConstantForce->FinishVelocityParams.Mode = ERootMotionFinishVelocityMode::ClampVelocity;
		ConstantForce->FinishVelocityParams.ClampVelocity = 1000.f;

		uint16 RootMotionSourceID = GetCharacterMovement()->ApplyRootMotionSource(ConstantForce);

	}
}

void ANanoCharacter::HideCameraIfCharacterClose()
{
	if (!IsLocallyControlled()) return;
	if ((FollowCamera->GetComponentLocation() - GetActorLocation()).Size() < CameraThreshold)
	{
		GetMesh()->SetVisibility(false);
		if (Combat && Combat->EquippedWeapon && Combat->EquippedWeapon->GetWeaponMesh())
		{
			Combat->EquippedWeapon->GetWeaponMesh()->bOwnerNoSee = true;
		}
	}
	else
	{
		GetMesh()->SetVisibility(true);
		if (Combat && Combat->EquippedWeapon && Combat->EquippedWeapon->GetWeaponMesh())
		{
			Combat->EquippedWeapon->GetWeaponMesh()->bOwnerNoSee = false;
		}
	}
}

void ANanoCharacter::UpdateHealthUI(float Alpha)
{
	HealthUI = UKismetMathLibrary::Lerp(HealthUI, Health, Alpha);
}

void ANanoCharacter::StartHealthTransition()
{
	HealthTrack.BindDynamic(this, &ANanoCharacter::UpdateHealthUI);
	if (HealthCurve && CommonTimeline)
	{
		CommonTimeline->AddInterpFloat(HealthCurve, HealthTrack);
		CommonTimeline->PlayFromStart();
	}
}

void ANanoCharacter::OnRep_Health(float LastHealth)
{
	UpdateHUDHealth();
}

void ANanoCharacter::OnRep_HealthUI()
{
	UpdateHUDHealth();
}

void ANanoCharacter::OnRep_Energy()
{
	UpdateHUDEnergy();
}

void ANanoCharacter::OnRep_Room()
{
	UpdateHUDRoom();
}

void ANanoCharacter::OnRep_Enforce()
{
	UpdateHUDEnforce();
}

void ANanoCharacter::DieTimerFinished()
{
	NanoGameMode = NanoGameMode == nullptr ? GetWorld()->GetAuthGameMode<ANanoGameMode>() : NanoGameMode;
	if (NanoGameMode && !bLeftGame)
	{
		NanoGameMode->RequestRespawn(this, Controller);
	}

}

void ANanoCharacter::UpdateDissolveMaterial(float DissolveValue)
{
	if (DynamicDissolveMaterialInstance)
	{
		DynamicDissolveMaterialInstance->SetScalarParameterValue(TEXT("Dissolve"), DissolveValue);
	}
}

void ANanoCharacter::StartDissolve()
{
	DissolveTrack.BindDynamic(this, &ANanoCharacter::UpdateDissolveMaterial);
	if (DissolveCurve && CommonTimeline)
	{
		UE_LOG(LogTemp, Warning, TEXT("DissolveTimeline"));
		CommonTimeline->AddInterpFloat(DissolveCurve, DissolveTrack);
		CommonTimeline->Play();
	}
}

