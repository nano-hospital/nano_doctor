// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "NanoPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class NANO_DOCTOR_API ANanoPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	void SetHUDHealth(float Health, float MaxHealth);
	void SetHUDEnergy(float Energy, float MaxEnergy);
	void SetHUDRoom(int Room);
	void SetHUDEnforce(int Enforce);

	virtual void OnPossess(APawn* InPawn) override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	class UCharacterOverlay* GetCharacterOverlay();
protected:
	virtual void BeginPlay() override;
	void PollInit();

private:
	UPROPERTY()
	class ANanoHUD* NanoHUD;

	UPROPERTY()
	UCharacterOverlay* CharacterOverlay;
	
	float HUDHealth;
	bool bInitializeHealth = false;
	float HUDMaxHealth;

	float HUDEnergy;
	bool bInitializeEnergy = false;
	float HUDMaxEnergy;

	int HUDRoom;
	bool bInitializeRoom = false;

	int HUDEnforce;
	bool bInitializeEnforce = false;
};
