// Fill out your copyright notice in the Description page of Project Settings.


#include "NanoPlayerController.h"

#include "NanoCharacter.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include "nano_doctor/HUD/CharacterOverlay.h"
#include "nano_doctor/HUD/NanoHUD.h"

void ANanoPlayerController::SetHUDHealth(float Health, float MaxHealth)
{
	NanoHUD = NanoHUD == nullptr ? Cast<ANanoHUD>(GetHUD()) : NanoHUD;
	bool bHUDValid = NanoHUD &&
		NanoHUD->CharacterOverlay &&
		NanoHUD->CharacterOverlay->HealthBar &&
		NanoHUD->CharacterOverlay->HealthText;
	if (bHUDValid)
	{
		const float HealthPercent = Health / MaxHealth;
		NanoHUD->CharacterOverlay->HealthBar->SetPercent(HealthPercent);
		FString HealthText = FString::Printf(TEXT("%d/%d"), FMath::CeilToInt(Health), FMath::CeilToInt(MaxHealth));
		NanoHUD->CharacterOverlay->HealthText->SetText(FText::FromString(HealthText));
	}
	else
	{
		bInitializeHealth = true;
		HUDHealth = Health;
		HUDMaxHealth = MaxHealth;
	}
}

void ANanoPlayerController::SetHUDEnergy(float Energy, float MaxEnergy)
{
	NanoHUD = NanoHUD == nullptr ? Cast<ANanoHUD>(GetHUD()) : NanoHUD;
	bool bHUDValid = NanoHUD &&
		NanoHUD->CharacterOverlay &&
		NanoHUD->CharacterOverlay->EnergyBar;
	if (bHUDValid)
	{
		const float EnergyPercent = Energy / MaxEnergy;
		NanoHUD->CharacterOverlay->EnergyBar->SetPercent(EnergyPercent);
	}
	else
	{
		bInitializeEnergy = true;
		HUDEnergy = Energy;
		HUDMaxEnergy = MaxEnergy;
	}
}

void ANanoPlayerController::SetHUDRoom(int Room)
{
	NanoHUD = NanoHUD == nullptr ? Cast<ANanoHUD>(GetHUD()) : NanoHUD;
	bool bHUDValid = NanoHUD &&
		NanoHUD->CharacterOverlay &&
		NanoHUD->CharacterOverlay->RoomText;
	if (bHUDValid)
	{
		FString RoomText = FString::Printf(TEXT("%d"), Room);
		UE_LOG(LogTemp, Warning, TEXT("roomtext: %s"), *RoomText);
		NanoHUD->CharacterOverlay->RoomText->SetText(FText::FromString(RoomText));
	}
	else
	{
		bInitializeRoom= true;
		HUDRoom = Room;
	}
}

void ANanoPlayerController::SetHUDEnforce(int Enforce)
{
	NanoHUD = NanoHUD == nullptr ? Cast<ANanoHUD>(GetHUD()) : NanoHUD;
	bool bHUDValid = NanoHUD &&
		NanoHUD->CharacterOverlay &&
		NanoHUD->CharacterOverlay->EnforceText;
	if (bHUDValid)
	{
		FString EnforceText = FString::Printf(TEXT("%d"), Enforce);
		UE_LOG(LogTemp, Warning, TEXT("Enforce: %s"), *EnforceText);
		NanoHUD->CharacterOverlay->EnforceText->SetText(FText::FromString(EnforceText));
	}
	else
	{
		bInitializeEnforce= true;
		HUDEnforce = Enforce;
	}
}

void ANanoPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	ANanoCharacter* NanoCharacter = Cast<ANanoCharacter>(InPawn);
	if (NanoCharacter)
	{
		SetHUDHealth(NanoCharacter->GetHealth(), NanoCharacter->GetMaxHealth());
		SetHUDEnergy(NanoCharacter->GetEnergy(), NanoCharacter->GetMaxEnergy());
		SetHUDRoom(NanoCharacter->GetRoom());
		SetHUDEnforce(NanoCharacter->GetEnforce());
	}
}

void ANanoPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	PollInit();
}

UCharacterOverlay* ANanoPlayerController::GetCharacterOverlay()
{
	return NanoHUD->CharacterOverlay;
}

void ANanoPlayerController::BeginPlay()
{
	Super::BeginPlay();

	NanoHUD = Cast<ANanoHUD>(GetHUD());
}

void ANanoPlayerController::PollInit()
{
	if (CharacterOverlay == nullptr)
	{
		if (NanoHUD && NanoHUD->CharacterOverlay)
		{
			CharacterOverlay = NanoHUD->CharacterOverlay;
			if (CharacterOverlay)
			{
				if (bInitializeHealth) SetHUDHealth(HUDHealth, HUDMaxHealth);
				if (bInitializeEnergy) SetHUDEnergy(HUDEnergy, HUDMaxEnergy);
				if (bInitializeRoom) SetHUDRoom(HUDRoom);
				if (bInitializeEnforce) SetHUDEnforce(HUDEnforce);
			}
		}
	}
}
