// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/TimelineComponent.h"
#include "GameFramework/Character.h"
#include "nano_doctor/Weapon/WeaponTypes.h"
#include "NanoCharacter.generated.h"

UCLASS()
class NANO_DOCTOR_API ANanoCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, Category=Camera)
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, Category=Camera)
	class UCameraComponent* FollowCamera;

	UPROPERTY(ReplicatedUsing = OnRep_OverlappingWeapon)
	class AWeapon* OverlappingWeapon;

	/**
	 * Nano Component
	 */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCombatComponent* Combat;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UBuffComponent* Buff;
	/**
	* Animation montages
	*/
	UPROPERTY(EditAnywhere, Category = Combat)
	UAnimMontage* FireWeaponMontage;

	UPROPERTY(EditAnywhere, Category = Combat)
	UAnimMontage* HitReactMontage;

	UPROPERTY(EditAnywhere, Category = Combat)
	UAnimMontage* DieMontage;
	
public:
	// Sets default values for this character's properties
	ANanoCharacter();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void PostInitializeComponents() override;

	/**
	* Play montages
	*/
	void PlayFireMontage(bool bAiming);

	void PlayHitReactMontage();

	void PlayDieMontage();

	UPROPERTY(BlueprintReadOnly, Replicated)
	bool bDisableGameplay = false;
	/**
	 * HUD 更新
	 */
	void UpdateHUDHealth();
	void UpdateHUDEnergy();
	void UpdateHUDRoom();
	void UpdateHUDEnforce();
	
	/**
	 * 玩家死亡
	 */
	void Die(bool bPlayerLeftGame);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastDie(bool bPlayerLeftGame);
	
	virtual void Destroyed() override;
	
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	bool CanFire();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void ResetDash();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnRep_OverlappingWeapon(AWeapon* LastWeapon);

	UFUNCTION(BlueprintCallable)
	void EquipButtonPressed();
	
	UFUNCTION(BlueprintCallable)
	void AimButtonPressed();
	
	UFUNCTION(BlueprintCallable)
	void AimButtonReleased();

	UFUNCTION(BlueprintCallable)
	void FireButtonPressed();
	
	UFUNCTION(BlueprintCallable)
	void FireButtonReleased();

	UFUNCTION(BlueprintCallable)
	void DropButtonPressed();
	
	void AimOffset(float DeltaTime);
	void CalculateAO_Pitch();

	/**
	 * RPC
	 */
	UFUNCTION(Server, Reliable)
	void ServerEquipButtonPressed();

	UFUNCTION(Server, Reliable)
	void ServerDropButtonPressed();

	UFUNCTION(BlueprintCallable)
	void ReceiveDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, class AController* InstigatorController, AActor* DamageCauser);

	UFUNCTION(BlueprintCallable)
	void PotionDamage(float Damage);
	
	void PollInit();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetOverlappingWeapon(AWeapon* Weapon);

	UFUNCTION(BlueprintCallable)
	bool IsWeaponEquipped() const;
	bool IsAiming();

	FORCEINLINE float GetAO_Yaw() const { return AO_Yaw; }
	FORCEINLINE float GetAO_Pitch() const { return AO_Pitch; }

	UFUNCTION(BlueprintCallable)
	AWeapon* GetEquippedWeapon() const;
	FVector GetHitTarget() const;

	UFUNCTION(BlueprintCallable)
	EWeaponType GetEquippedWeaponType() const;

	void PickupRoom();
	
	UFUNCTION(BlueprintCallable)
	void UseRoom();

	void PickupEnforce();
	
	UFUNCTION(BlueprintCallable)
	void UseEnforce();

	UFUNCTION(BlueprintCallable)
	FORCEINLINE UCameraComponent* GetFollowCamera() const { return FollowCamera; }
	FORCEINLINE bool IsDied() const { return bDied; }
	UFUNCTION(BlueprintCallable)
	FORCEINLINE float GetHealth() const { return Health; }
	FORCEINLINE void SetHealth(float Amount) { Health = Amount; }
	FORCEINLINE float GetMaxHealth() const { return MaxHealth; }
	FORCEINLINE UCombatComponent* GetCombat() const { return Combat; }
	FORCEINLINE float GetEnergy() const { return Energy; }

	UFUNCTION(NetMulticast, Reliable)
	FORCEINLINE void SetEnergy(float Amount);
	FORCEINLINE float GetMaxEnergy() const { return MaxEnergy; }
	UFUNCTION(BlueprintCallable)
	FORCEINLINE UBuffComponent* GetBuff() const { return Buff; }
	FORCEINLINE int GetRoom() const { return CarryRoom; }
	FORCEINLINE int GetEnforce() const { return CarryEnforce; }

	UFUNCTION(BlueprintCallable)
	void ApplyRootMotionConstantForce(FVector WorldDirection, float Strength, float Duration);

	UPROPERTY(BlueprintReadWrite, Replicated)
	bool bIsImmune = false;
private:
	float AO_Yaw;
	float AO_Pitch;
	FRotator StartingAimRotation;

	void HideCameraIfCharacterClose();
	
	UPROPERTY(EditAnywhere)
	float CameraThreshold = 200.f;

	/**
	* Player health
	*/

	UPROPERTY(EditAnywhere, Category = "Player Stats")
	float MaxHealth = 100.f;

	UPROPERTY(BlueprintReadWrite, ReplicatedUsing = OnRep_Health, VisibleAnywhere, Category = "Player Stats", meta=(AllowPrivateAccess="true"))
	float Health = 100.f;
	
	UPROPERTY(BlueprintReadWrite, ReplicatedUsing = OnRep_HealthUI, Category = "Player Stats", meta=(AllowPrivateAccess="true"))
	float HealthUI = 100.f;

	UPROPERTY(BlueprintReadWrite, ReplicatedUsing = OnRep_Energy, meta=(AllowPrivateAccess="true"))
	float Energy = 100.f;
	
	UPROPERTY(BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	float MaxEnergy = 100.f;
	
	UPROPERTY(BlueprintReadWrite, ReplicatedUsing = OnRep_Room, meta=(AllowPrivateAccess="true"))
	int CarryRoom = 0;

	UPROPERTY(BlueprintReadWrite, ReplicatedUsing = OnRep_Enforce, meta=(AllowPrivateAccess="true"))
	int CarryEnforce = 0;
	
	UPROPERTY(EditAnywhere)
	UCurveFloat* HealthCurve;

	FOnTimelineFloat HealthTrack;

	UFUNCTION()
	void UpdateHealthUI(float Alpha);
	void StartHealthTransition();
	
	UFUNCTION()
	void OnRep_Health(float LastHealth);

	UFUNCTION()
	void OnRep_HealthUI();

	UFUNCTION()
	void OnRep_Energy();
	
	UFUNCTION()
	void OnRep_Room();

	UFUNCTION()
	void OnRep_Enforce();
	
	UPROPERTY(BlueprintReadOnly, meta=(AllowPrivateAccess="true"))
	class ANanoPlayerController* NanoPlayerController;

	bool bDied = false;

	FTimerHandle DieTimer;

	UPROPERTY(EditDefaultsOnly)
	float DieDelay = 3.f;

	void DieTimerFinished();

	bool bLeftGame = false;
	
	UPROPERTY()
	class ANanoGameMode* NanoGameMode;

	/**
	* Dissolve effect
	*/

	UPROPERTY(VisibleAnywhere)
	UTimelineComponent* CommonTimeline;
	FOnTimelineFloat DissolveTrack;

	UPROPERTY(EditAnywhere)
	UCurveFloat* DissolveCurve;

	UFUNCTION()
	void UpdateDissolveMaterial(float DissolveValue);
	void StartDissolve();

	// Dynamic instance that we can change at runtime
	UPROPERTY(VisibleAnywhere, Category = Die)
	UMaterialInstanceDynamic* DynamicDissolveMaterialInstance;

	// Material instance set on the Blueprint, used with the dynamic material instance
	UPROPERTY(EditAnywhere, Category = Die)
	UMaterialInstance* DissolveMaterialInstance;
};
