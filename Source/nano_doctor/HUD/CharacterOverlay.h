// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/VerticalBox.h"
#include "CharacterOverlay.generated.h"

/**
 * 
 */
UCLASS()
class NANO_DOCTOR_API UCharacterOverlay : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;
	
	UPROPERTY(BlueprintReadOnly)
	class ANanoCharacter* NanoCharacter;
	
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UProgressBar* HealthBar;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* HealthText;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UProgressBar* EnergyBar;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* RoomText;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* EnforceText;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UVerticalBox* BossBarBox;
};
